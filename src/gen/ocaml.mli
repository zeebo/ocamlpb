module rec Module : sig
	type t = {
		name : string ;
		modules : Module.t list ;
		types : Type.t list ;
	}
end
and Type : sig
	module rec Record : sig
		module rec Field : sig
			type t = {
				name : string ;
				type_ : Type.product ;
			}
		end
		type t = {
			name : string ;
			fields : Field.t list ;
		}
	end
	and Enum : sig
		type t = {
			name : string ;
			cases : case list ;
		}
		and case = {
			variant : string ;
			type_ : Type.product option ;
		}
	end
	and Named : sig
		type t = {
			name : string ;
			type_ : Type.product ;
		}
	end
	type t =
		| Record of Record.t
		| Enum of Enum.t
		| Named of Named.t
	and ref = {
		modules : string list ;
		name : string ;
		mods : string list ;
	}
	and product = ref list
end

val write_modules : Buffer.t -> Module.t list -> unit