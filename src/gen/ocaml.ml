module rec Module : sig
	type t = {
		name : string ;
		modules : Module.t list ;
		types : Type.t list ;
	}
end = Module
and Type : sig
	module rec Record : sig
		module rec Field : sig
			type t = {
				name : string ;
				type_ : Type.product ;
			}
		end
		type t = {
			name : string ;
			fields : Field.t list ;
		}
	end
	and Enum : sig
		type t = {
			name : string ;
			cases : case list ;
		}
		and case = {
			variant : string ;
			type_ : Type.product option ;
		}
	end
	and Named : sig
		type t = {
			name : string ;
			type_ : Type.product ;
		}
	end
	type t =
		| Record of Record.t
		| Enum of Enum.t
		| Named of Named.t
	and ref = {
		modules : string list ;
		name : string ;
		mods : string list ;
	}
	and product = ref list
end = Type


let is_ocaml_keyword word = match word with
	| "and" | "as" | "assert" | "begin" | "class" | "constraint" | "do"
	| "done" | "downto" | "else" | "end" | "exception" | "external" | "false"
	| "for" | "fun" | "function" | "functor" | "if" | "in" | "include"
	| "inherit" | "initializer" | "lazy" | "let" | "match" | "method"
	| "module" | "mutable" | "new" | "nonrec" | "object" | "of" | "open"
	| "or" | "private" | "rec" | "sig" | "struct" | "then" | "to" | "true"
	| "try" | "type" | "val" | "virtual" | "when" | "while" | "with" | "mod"
	| "land" | "lor" | "lxor" | "lsl" | "lsr" | "asr" -> true
	| _     -> false

let ensure_valid_name name =
	if is_ocaml_keyword name then name ^ "_" else name

(*
 * TODO(jeff): i don't know of a better way to to do this. just printf with
 * a number of indents i guess :( seems like there's a better more structured
 * way that would let me stop thinking about whitespace so much. oh well.
 *)

let string_of_ref ref = Type.(
	let name = String.concat "." @@ ref.modules @ [ref.name] in
	match ref.mods with
	| [] -> name;
	| mods ->
		let joined_mods = String.concat " " ref.mods in
		name ^ " " ^ joined_mods;
	;
)

let string_of_product product =
	String.concat " * " @@ List.map string_of_ref product

let write_indented buf indent pf =
	Buffer.add_string buf @@ String.make indent '\t';
	Printf.bprintf buf pf

let write_record_field buf indent field = Type.Record.Field.(
	let valid_name = ensure_valid_name field.name in
	let type_string = string_of_product field.type_ in
	write_indented buf indent "%s : %s ;\n" valid_name type_string;
)

let write_record buf indent first record = Type.Record.(
	let valid_name = ensure_valid_name record.name in
	match first with
	| true -> write_indented buf indent "type %s = {\n" valid_name;
	| false -> write_indented buf indent "and %s = {\n" valid_name;
	;
	List.iter (write_record_field buf (indent + 1)) record.fields;
	write_indented buf indent "}\n";
)

let write_enum_case buf indent case = Type.Enum.(
	let valid_name = ensure_valid_name case.variant in
	match case.type_ with
	| None -> write_indented buf indent "| %s\n" valid_name;
	| Some type_ ->
		let type_string = string_of_product type_ in
		write_indented buf indent "| %s of %s\n" valid_name type_string;
	;
)

let write_enum buf indent first enum = Type.Enum.(
	let valid_name = ensure_valid_name enum.name in
	match first with
	| true -> write_indented buf indent "type %s =\n" valid_name;
	| false -> write_indented buf indent "and %s =\n" valid_name;
	;
	List.iter (write_enum_case buf (indent + 1)) enum.cases;
)

let write_named buf indent first named = Type.Named.(
	let valid_name = ensure_valid_name named.name in
	match first with
	| true -> write_indented buf indent "type %s = " valid_name;
	| false -> write_indented buf indent "and %s = " valid_name;
	;
	let type_string = string_of_product named.type_ in
	Printf.bprintf buf "%s\n" type_string;
)

let write_type buf indent first typ =
	match typ with
	| Type.Record record -> write_record buf indent first record
	| Type.Enum enum -> write_enum buf indent first enum
	| Type.Named named -> write_named buf indent first named

let iter_with_first fn ls =
	let rec iter fn ls state =
		match ls with
		| [] -> ()
		| head :: tail ->
			fn state head ;
			iter fn tail false
		;
	in
	iter fn ls true

let rec write_module buf indent top first module_ = Module.(
	let valid_name = ensure_valid_name module_.name in
	match first with
	| true -> write_indented buf indent "module rec %s : sig\n" valid_name;
	| false -> write_indented buf indent "and %s : sig\n" valid_name;
	;
	iter_with_first (write_module buf (indent + 1) false) module_.modules;
	iter_with_first (write_type buf (indent + 1)) module_.types;
	match top with
	| true -> write_indented buf indent "end = %s\n" valid_name;
	| false -> write_indented buf indent "end\n";
	;
)

let rec write_modules buf modules =
	iter_with_first (write_module buf 0 true) modules