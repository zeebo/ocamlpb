module rec FileDescriptorSet : sig
	type t = {
		file : FileDescriptorProto.t list ;
	}
end = FileDescriptorSet
and FileDescriptorProto : sig
	type t = {
		name : string option ;
		package : string option ;
		message_type : DescriptorProto.t list ;
		enum_type : EnumDescriptorProto.t list ;
	}
end = FileDescriptorProto
and DescriptorProto : sig
	type t = {
		name : string option ;
		field : FieldDescriptorProto.t list ;
		nested_type : DescriptorProto.t list ;
		enum_type : EnumDescriptorProto.t list ;
	}
end = DescriptorProto
and FieldDescriptorProto : sig
	module rec Type : sig
		type t =
			| TYPE_DOUBLE
			| TYPE_FLOAT
			| TYPE_INT64
			| TYPE_UINT64
			| TYPE_INT32
			| TYPE_FIXED64
			| TYPE_FIXED32
			| TYPE_BOOL
			| TYPE_STRING
			| TYPE_GROUP
			| TYPE_MESSAGE
			| TYPE_BYTES
			| TYPE_UINT32
			| TYPE_ENUM
			| TYPE_SFIXED32
			| TYPE_SFIXED64
			| TYPE_SINT32
			| TYPE_SINT64
	end
	and Label : sig
		type t =
			| LABEL_OPTIONAL
			| LABEL_REQUIRED
			| LABEL_REPEATED
	end
	type t = {
		name : string option ;
		number : Stdint.Int32.t option ;
		label : Label.t option ;
		type_ : Type.t option ;
		type_name : string option ;
		default_value : string option ;
	}
end = FieldDescriptorProto
and EnumDescriptorProto : sig
	type t = {
		name : string option ;
		value : EnumValueDescriptorProto.t list ;
	}
end = EnumDescriptorProto
and EnumValueDescriptorProto : sig
	type t = {
		name : string option ;
		number : Stdint.Int32.t option ;
	}
end = EnumValueDescriptorProto
