open Stdint ;;

exception Invalid_parse of string ;;

type field_value =
	| Varint of Uint64.t
	| LengthDelimited of bytes
;;

type field = int * field_value ;;

let encode_varint buf field_num value =
	Tags.encode buf (Tags.Varint, field_num);
	Varint.encode buf value;
;;

let encode_bytes buf field_num value =
	let length = Bytes.length value in
	Tags.encode buf (Tags.LengthDelimited, field_num);
	Varint.encode buf @@ Uint64.of_int length;
	Buffer.add_bytes buf value;
;;

let encode_field buf field =
	let field_num, field_value = field in
	match field_value with
	| Varint value -> encode_varint buf field_num value
	| LengthDelimited value -> encode_bytes buf field_num value
;;

let consume_length buf length =
	let buf_len = Bytes.length buf in
	if buf_len < length then
		raise (Invalid_parse "length too long for field")
	else
		let value = Bytes.sub buf 0 length in
		let buf = Bytes.sub buf length (buf_len - length) in
		(buf, value)
;;

let decode_field buf =
	let buf, (wiretype, field_num) = Tags.decode buf in
	match wiretype with
	| Tags.Varint ->
		let buf, value = Varint.decode buf in
		(buf, (field_num, Varint value))
	| Tags.LengthDelimited ->
		let buf, length = Varint.decode buf in
		let buf, value = consume_length buf @@ Uint64.to_int length in
		(buf, (field_num, LengthDelimited value))
;;

let marshal_into buf msg iter =
	let encode_field_to_buf field = encode_field buf field in
	iter msg encode_field_to_buf
;;

let marshal msg iter =
	let buf = Buffer.create 1024 in
	marshal_into buf msg iter;
	Buffer.to_bytes buf
;;

let unmarshal buf msg update =
	let msg = ref msg in
	let buf = ref buf in
	while Bytes.length !buf > 0 do
		let rem, field = decode_field !buf in
		buf := rem;
		msg := update !msg field;
	done;
	!msg
;;
