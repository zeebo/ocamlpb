open Stdint ;;

type wiretype =
	| Varint
	| LengthDelimited
;;

let int_of_wiretype = function
	| Varint -> 0
	| LengthDelimited -> 2
;;

let wiretype_of_int = function
	| 0 -> Varint
	| 2 -> LengthDelimited
	| x -> invalid_arg @@ string_of_int x
;;

type tag = wiretype * int ;;

let mask   = Uint64.of_int 7 ;;
let shifts = 3 ;;

let ( lsr ) a b = Uint64.shift_right a b ;;
let ( lor ) a b = Uint64.logor a b ;;
let ( land ) a b = Uint64.logand a b ;;

let uint64_of_tag tag =
	let wiretype, field_num = tag in
	let shifted = field_num lsl shifts in
	let encoded_wt = int_of_wiretype wiretype in
	(Uint64.of_int shifted) lor (Uint64.of_int encoded_wt)
;;

let tag_of_uint64 value =
	let wiretype = Uint64.to_int @@ value land mask in
	let shifted  = Uint64.to_int @@ value lsr shifts in
	(wiretype_of_int wiretype, shifted)
;;

let encode buf tag =
	Varint.encode buf (uint64_of_tag tag)
;;

let decode encoded =
	let rest, decoded = Varint.decode encoded in
	(rest, tag_of_uint64 decoded)
;;