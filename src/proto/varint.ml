open Stdint ;;

let ( lor ) a b = Uint64.logor a b ;;
let ( land ) a b = Uint64.logand a b ;;
let ( lsr ) a b = Uint64.shift_right a b ;;
let ( lsl ) a b = Uint64.shift_left a b ;;

let char_of_uint64 a = Uint64.to_int a |> char_of_int ;;
let uint64_of_char a = int_of_char a |> Uint64.of_int ;;

let mask     = Uint64.of_int 127 ;;
let high_bit = Uint64.of_int 128 ;;
let bits     = 7 ;;

let encode buf input =
	let rec iter buf input =
		let masked = input land mask in
		if input > masked then (
			let bit_set = masked lor high_bit in
			Buffer.add_char buf @@ char_of_uint64 bit_set;
			iter buf (input lsr bits)
		) else
			Buffer.add_char buf @@ char_of_uint64 input;
	in
	iter buf input;
;;

let decode buf =
	let rec iter accum buf index =
		let byte = uint64_of_char @@ Bytes.get buf index in
		let masked = byte land mask in
		let shifted = masked lsl (bits * index) in
		let accum = accum lor shifted in
		if byte > mask then
			iter accum buf (index + 1)
		else
			(index + 1, accum)
	in
	let (read, decoded) = iter Uint64.zero buf 0 in
	let len = Bytes.length buf in
	(Bytes.sub buf read (len - read), decoded)
;;
