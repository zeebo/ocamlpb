let mk_type modules name mods = Ocaml.Type.({
	modules = modules ;
	name = name ;
	mods = mods ;
})

let type_ name = Ocaml.Type.({
	modules = [] ;
	name = name ;
	mods = [] ;
})

let list_of   type_ = Ocaml.Type.({ type_ with mods = ["list"] })
let option_of type_ = Ocaml.Type.({ type_ with mods = ["option"] })

let field name type_ = Ocaml.Type.Record.Field.({
	name = name ;
	type_ = [type_] ;
})

let case variant type_ = Ocaml.Type.Enum.({
	variant = variant ;
	type_ = Some [type_] ;
})

let empty_case variant = Ocaml.Type.Enum.({
	variant = variant ;
	type_ = None ;
})


let main () =
	let buf = Buffer.create 4096 in
	let module_root_ref module_ = mk_type [module_] "t" [] in

	let root_record fields = Ocaml.Type.(Record Record.({
		name = "t" ;
		fields = fields ;
	})) in
	let root_enum cases = Ocaml.Type.(Enum Enum.({
		name = "t" ;
		cases = cases ;
	})) in

	let optional_string = option_of @@ type_ "string" in

	Ocaml.write_modules buf Ocaml.([
		Module.({
			name = "FileDescriptorSet" ;
			modules = [] ;
			types = [root_record [
				field "file" @@ list_of @@ module_root_ref "FileDescriptorProto" ;
			]] ;
		}) ;

		Module.({
			name = "FileDescriptorProto" ;
			modules = [] ;
			types = [root_record [
				field "name" optional_string ;
				field "package" optional_string ;
				field "message_type" @@ list_of @@ module_root_ref "DescriptorProto" ;
				field "enum_type" @@ list_of @@ module_root_ref "EnumDescriptorProto" ;
			]] ;
		}) ;

		Module.({
			name = "DescriptorProto" ;
			modules = [] ;
			types = [root_record [
				field "name" optional_string ;
				field "field" @@ list_of @@ module_root_ref "FieldDescriptorProto" ;
				field "nested_type" @@ list_of @@ module_root_ref "DescriptorProto" ;
				field "enum_type" @@ list_of @@ module_root_ref "EnumDescriptorProto" ;
			]] ;
		}) ;

		Module.({
			name = "FieldDescriptorProto" ;
			modules = [
				Module.({
					name = "Type" ;
					modules = [] ;
					types = [root_enum [
						empty_case "TYPE_DOUBLE" ;
						empty_case "TYPE_FLOAT" ;
						empty_case "TYPE_INT64" ;
						empty_case "TYPE_UINT64" ;
						empty_case "TYPE_INT32" ;
						empty_case "TYPE_FIXED64" ;
						empty_case "TYPE_FIXED32" ;
						empty_case "TYPE_BOOL" ;
						empty_case "TYPE_STRING" ;
						empty_case "TYPE_GROUP" ;
						empty_case "TYPE_MESSAGE" ;
						empty_case "TYPE_BYTES" ;
						empty_case "TYPE_UINT32" ;
						empty_case "TYPE_ENUM" ;
						empty_case "TYPE_SFIXED32" ;
						empty_case "TYPE_SFIXED64" ;
						empty_case "TYPE_SINT32" ;
						empty_case "TYPE_SINT64" ;
					]] ;
				}) ;
				Module.({
					name = "Label" ;
					modules = [] ;
					types = [root_enum [
						empty_case "LABEL_OPTIONAL" ;
						empty_case "LABEL_REQUIRED" ;
						empty_case "LABEL_REPEATED" ;
					]] ;
				}) ;
			] ;
			types = [root_record [
				field "name" optional_string ;
				field "number" @@ option_of @@ mk_type ["Stdint"; "Int32"] "t" [] ;
				field "label" @@ option_of @@ module_root_ref "Label" ;
				field "type" @@ option_of @@ module_root_ref "Type" ;
				field "type_name" optional_string ;
				field "default_value" optional_string ;
			]] ;
		}) ;

		Module.({
			name = "EnumDescriptorProto" ;
			modules = [] ;
			types = [root_record [
				field "name" optional_string ;
				field "value" @@ list_of @@ module_root_ref "EnumValueDescriptorProto" ;
			]] ;
		}) ;

		Module.({
			name = "EnumValueDescriptorProto" ;
			modules = [] ;
			types = [root_record [
				field "name" optional_string ;
				field "number" @@ option_of @@ mk_type ["Stdint"; "Int32"] "t" [] ;
			]] ;
		}) ;

	]) ;
	Printf.printf "%s" @@ Buffer.contents buf

let () =
	main ()