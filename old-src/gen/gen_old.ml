open Descriptor ;;
open Printf ;;

module Option = struct
	let get = function
	| Some v -> v
	| None -> invalid_arg "get on None"
	;;

	let map value fn = match value with
	| Some v -> Some (fn v)
	| None -> None
	;;

end ;;

exception Not_implemented_yet of string ;;

type p_file = {
	file_name : string ;
	messages : p_message list ;
	enums : p_enum list ;
}

and p_fq_type = FQType of string * string

and p_message = {
	message_name : string ;
	message_fq_type : p_fq_type ;
	fields : p_field list ;
	sub_messages : p_message list ;
	sub_enums : p_enum list ;
}

and p_enum = {
	enum_name : string ;
	enum_fq_type : p_fq_type ;
	values : p_enum_value list ;
}

and p_enum_value = {
	enum_value_name : string ;
	value : Int32.t ;
}

and p_field = {
	name : string ;
	field_number : Int32.t ;
	label : p_label ;
	field_type : p_field_type ;
	default : string option ;
}

and p_label =
	| LOptional
	| LRequired
	| LRepeated

and p_field_type =
	| FInt64
	| FUint64
	| FInt32
	| FUint32
	| FString
	| FBytes
	| FDouble
	| FBool
	| FToplevel of p_toplevel_type

(* always fully qualified types separated by _ *)
and p_toplevel_type = p_toplevel_kind * p_fq_type

and p_toplevel_kind =
	| TEnum
	| TMessage

and p_type_tree = TypeTree of (string * p_toplevel_kind * p_type_tree) list

and p_wiretype =
	| WVarint
	| WLengthDelimited

and p_selector = 
	| SFullyQualified of string list
	| SRelative of string list

;;

(* fq_type methods *)

let string_of_fq_type (FQType (name, fq)) kind = begin
	sprintf "p_%s_%s_%s" name fq kind
end ;;

let fq_type_name (FQType (name, fq)) = begin
	sprintf "p_%s_%s" name fq
end ;;

let fq_type_empty_name fq_type = begin
	string_of_fq_type fq_type "empty"
end ;;

let fq_type_update_name fq_type = begin
	string_of_fq_type fq_type "update"
end ;;

let fq_type_iter_name fq_type = begin
	string_of_fq_type fq_type "iter"
end ;;

let fq_type_of_int_name fq_type = begin
	string_of_fq_type fq_type "of_int"
end  ;;

let fq_type_to_int_name fq_type = begin
	string_of_fq_type fq_type "to_int"
end  ;;

(* field methods *)

let field_type_name field = begin
	let base_type = match field.field_type with
	| FInt64 -> "Int64.t"
	| FUint64 -> "Uint64.t"
	| FInt32 -> "Int32.t"
	| FUint32 -> "Uint32.t"
	| FString -> "string"
	| FBytes -> "bytes"
	| FDouble -> "float"
	| FBool -> "bool"
	| FToplevel (_, fq_type) -> fq_type_name fq_type
	in let modifier = match field.label with
	| LOptional -> "option"
	| LRequired -> ""
	| LRepeated -> "list"
	in sprintf "%s %s" base_type modifier
end ;;

let field_zero_value field = begin
	match field.label with
	| LOptional -> "None"
	| LRepeated -> "[]"
	| LRequired -> raise (Not_implemented_yet "required zero values")
end ;;

let field_type_of_wiretype_expr field_type value_name = begin
	match field_type with
	| FInt64 -> sprintf "(Int64.of_uint64 %s)" value_name
	| FUint64 -> value_name
	| FInt32 -> sprintf "(Int32.of_uint64 %s)" value_name
	| FUint32 -> sprintf "(Uint32.of_uint64 %s)" value_name
	| FString -> sprintf "(Bytes.to_string %s)" value_name
	| FBytes -> value_name
	| FDouble -> value_name (* TODO: fix this *)
	| FBool -> sprintf "(Uint64.compare %s Uint64.zero != 0)" value_name
	| FToplevel (TEnum, fq_type) ->
		sprintf "(%s @@ Uint64.to_int %s)"
			(fq_type_of_int_name fq_type)
			value_name
	| FToplevel (TMessage, fq_type) ->
		sprintf "(Proto.unmarshal %s %s %s)"
			value_name 
			(fq_type_empty_name fq_type)
			(fq_type_update_name fq_type)
end ;;

let wiretype_of_field_type_expr field_type value_name = begin
	match field_type with
	| FInt64 -> sprintf "(Uint64.of_int64 %s)" value_name
	| FUint64 -> value_name
	| FInt32 -> sprintf "(Uint64.of_int32 %s)" value_name
	| FUint32 -> sprintf "(Uint64.of_uint32 %s)" value_name
	| FString -> sprintf "(Bytes.of_string %s)" value_name
	| FBytes -> value_name
	| FDouble -> value_name (* TOOD: fix this *)
	| FBool -> sprintf "(if %s then Uint64.one else Uint64.zero end)" value_name
	| FToplevel (TEnum, fq_type) ->
		sprintf "(Uint64.of_int @@ %s %s)"
			(fq_type_to_int_name fq_type)
			value_name
	| FToplevel (TMessage, fq_type) ->
		sprintf "(Proto.marshal %s %s)"
			value_name
			(fq_type_iter_name fq_type)
end ;;

let wiretype_of_field_type field_type = begin
	match field_type with
	| FInt64 -> WVarint
	| FUint64 -> WVarint
	| FInt32 -> WVarint
	| FUint32 -> WVarint
	| FString -> WLengthDelimited
	| FBytes -> WLengthDelimited
	| FDouble -> WVarint (* TODO: fix this *)
	| FBool -> WVarint
	| FToplevel (TEnum, _) -> WVarint
	| FToplevel (TMessage, _) -> WLengthDelimited
end ;;

let proto_expr_of_wiretype wiretype = begin
	match wiretype with
	| WVarint -> "Proto.Varint"
	| WLengthDelimited -> "Proto.LengthDelimited"
end ;;

(* type tree methods *)

let concat_type_trees trees = begin
	let rec loop trees accum =
		match trees with
		| TypeTree values :: tail ->
			loop tail @@ List.append accum values
		| [] -> accum
	in TypeTree (loop trees [])
end ;;

let map_type_trees fn iters = begin
	concat_type_trees @@ List.map fn iters
end ;;

let construct_type_tree file = begin
	let rec cons_message message = begin
		let child_messages = map_type_trees cons_message message._nested_type
		in let child_enums = map_type_trees cons_enum message._enum_type
		in let name = match message._name with
			| Some name -> name
			| None -> invalid_arg "name is required"
		in let children = concat_type_trees [child_messages; child_enums]
		in TypeTree [(name, TMessage, children)]
	end and cons_enum enum = begin
		let name = match enum._name with
			| Some name -> name
			| None -> invalid_arg "name is required"
		in TypeTree [(name, TEnum, TypeTree [])]
	end in

	let messages = map_type_trees cons_message file._message_type
	in let enums = map_type_trees cons_enum file._enum_type
	in concat_type_trees [messages; enums]
end ;;

let construct_type_trees file_set = begin
	map_type_trees construct_type_tree file_set._file
end ;;

let selector_of_string selector = begin
	let rec index str sep = begin
		try String.index str sep with | _ -> -1
	end and partition str index = begin
		let length = String.length str in
		String.sub str 0 index, String.sub str (index + 1) (length - index - 1)
	end and split_string str sep = begin
		let rec loop accum str sep = begin
			match index str sep with
			| index when index >= 0 ->
				let part, rem = partition str index in
				loop (part :: accum) rem sep
			| _ -> str :: accum
		end in List.rev @@ loop [] str sep
	end in

	let parts = split_string selector '.'
	in match parts with
	| "" :: rest -> SFullyQualified rest
	| _ -> SRelative parts
end ;;

let sanitize_file_name file_name = begin
	try String.sub file_name 0 (String.index file_name '.')
	with | _ -> file_name
end

let fq_type_of_stack file_name stack = begin
	FQType (sanitize_file_name file_name, (String.concat "_" @@ List.rev stack))
end ;;

let fq_type_of_selector file_name root_type_trees stack selector = begin
	match selector with
	| SFullyQualified path -> fq_type_of_stack file_name @@ List.rev path
	(* check immediate children on the type_stack and pop until found *)
	| SRelative parts -> raise (Not_implemented_yet "relative resolution")
end ;;

(* parsing from descriptor methods *)

let file_of_file_descriptor file = begin
	let file_name = match file._name with
	| Some file_name -> file_name
	| None -> invalid_arg "name is required"
	in let root_type_tree = construct_type_tree file
	in let rec cons_message stack (message : Descriptor.pDescriptorProto) = begin
		let name = match message._name with
		| Some name -> name
		| None -> invalid_arg "name is required"
		in let stack = name :: stack
		in let sub_messages = List.map (cons_message stack) message._nested_type
		in let sub_enums = List.map (cons_enum stack) message._enum_type
		in let fields = List.map (cons_field stack) message._field
		in {
			message_name = name ;
			message_fq_type = fq_type_of_stack file_name stack ;
			fields = fields ;
			sub_messages = sub_messages ;
			sub_enums = sub_enums ;
		}
	end and resolve_field_type stack (field : Descriptor.pFieldDescriptorProto) kind = begin
		let type_name = match field._type_name with
			| Some type_name -> type_name
			| None -> invalid_arg "type_name is required when type is Message or Enum"
		in let selector = selector_of_string type_name
		in let fq = fq_type_of_selector file_name root_type_tree stack selector
		in FToplevel (kind, fq)
	end and cons_field stack (field : Descriptor.pFieldDescriptorProto) = begin
		let name = match field._name with
			| Some name -> name
			| None -> invalid_arg "name is required"
		in let number = match field._number with
			| Some number -> number
			| None -> invalid_arg "number is required"
		in let label = match field._label with
			| Some Descriptor.LABEL_OPTIONAL -> LOptional
			| Some Descriptor.LABEL_REQUIRED -> LRequired
			| Some Descriptor.LABEL_REPEATED -> LRepeated
			| None -> invalid_arg "number is required"
		in let field_type = match field._type with
			| Some Descriptor.TYPE_INT64 -> FInt64
			| Some Descriptor.TYPE_UINT64 -> FUint64
			| Some Descriptor.TYPE_INT32 -> FInt32
			| Some Descriptor.TYPE_UINT32 -> FUint32
			| Some Descriptor.TYPE_STRING -> FString
			| Some Descriptor.TYPE_BYTES -> FBytes
			| Some Descriptor.TYPE_DOUBLE -> FDouble
			| Some Descriptor.TYPE_BOOL -> FBool
			| Some Descriptor.TYPE_MESSAGE -> 
				resolve_field_type stack field TMessage
			| Some Descriptor.TYPE_ENUM ->
				resolve_field_type stack field TEnum
			| None -> invalid_arg "field type is required"
			| _ -> invalid_arg (sprintf "unknown field type: %d" (Descriptor.pFieldDescriptorProto_Type_to_int @@ Option.get @@ field._type))
		in {
			name = name ;
			field_number = number ;
			label = label ;
			field_type = field_type ;
			default = field._default_value ;
		}
	end and cons_enum_value stack (value : Descriptor.pEnumValueDescriptorProto) = begin
		let name = match value._name with
			| Some name -> name
			| None -> invalid_arg "name is required"
		in let value = match value._number with
			| Some value -> value
			| None -> invalid_arg "value is required"
		in {
			enum_value_name = name ;
			value = value;
		}
	end and cons_enum stack (enum : Descriptor.pEnumDescriptorProto) = begin
		let name = match enum._name with
			| Some name -> name
			| None -> invalid_arg "name is required"
		in let stack = name :: stack
		in let values = List.map (cons_enum_value stack) enum._value
		in {
			enum_name = name ;
			enum_fq_type = fq_type_of_stack file_name stack ;
			values = values ;
		}
	end in

	let messages = List.map (cons_message []) file._message_type
	in let enums = List.map (cons_enum []) file._enum_type
	in {
		file_name = file_name;
		messages = messages;
		enums = enums;
	}
end ;;

let files_of_file_set_descriptor file_set = begin
	List.map file_of_file_descriptor file_set._file
end ;;

(* output methods *)

let rec output_message_type message = begin
	printf "and %s = {\n" (fq_type_name message.message_fq_type) ;
	printf "\tunknown : Proto.field list ;\n" ;
	let output_field field = begin
		printf "\t_%s : %s ;\n" field.name (field_type_name field)
	end in List.iter output_field message.fields;
	printf "}\n\n";

	List.iter output_message_type message.sub_messages;
	List.iter output_enum_type message.sub_enums;
end and output_enum_type enum = begin
	printf "and %s =\n" (fq_type_name enum.enum_fq_type) ;
	let output_enum_value value = begin
		printf "\t| %s\n" value.enum_value_name;
	end in List.iter output_enum_value enum.values;
	printf "\n";
end ;;

let output_types file = begin
	printf "type empty = unit\n\n" ;
	List.iter output_message_type file.messages;
	List.iter output_enum_type file.enums;
end ;;

let rec output_empty message = begin
	printf "and %s : %s = {\n"
		(fq_type_empty_name message.message_fq_type)
		(fq_type_name message.message_fq_type);
	printf "\tunknown = [];\n";
	let output_field field = begin
		printf "\t_%s = %s ;\n" field.name (field_zero_value field);
	end in List.iter output_field message.fields;
	printf "}\n\n";
end ;;

let update_msg field value_name = begin
	match field.label with
	| LOptional -> sprintf "{ msg with _%s = Some %s }" field.name value_name
	| LRepeated -> sprintf "{ msg with _%s = %s :: msg._%s }" field.name value_name field.name
	| LRequired -> sprintf "{ msg with _%s = %s }" field.name value_name
end ;;

let rec output_update message = begin
	printf "and %s (msg : %s) field = begin\n"
		(fq_type_update_name message.message_fq_type)
		(fq_type_name message.message_fq_type);
	printf "\tmatch field with\n";
	let output_field field = begin
		let number = Int32.to_string field.field_number
		in let wiretype = wiretype_of_field_type field.field_type
		in
		printf "\t| (%s, %s value) ->\n" number (proto_expr_of_wiretype wiretype);
		printf "\t\tlet decoded = %s in\n" (field_type_of_wiretype_expr field.field_type "value");
		printf "\t\t%s\n" (update_msg field "decoded");
		printf "\t| (%s, _) -> invalid_arg \"wrong type for field\"\n" number;
	end in List.iter output_field message.fields;
	printf "\t| _ -> { msg with unknown = field :: msg.unknown }\n";
	printf "end\n\n";
end ;;

let rec output_iter message = begin
	printf "and %s (msg : %s) fn = begin\n"
		(fq_type_iter_name message.message_fq_type)
		(fq_type_name message.message_fq_type);
	let output_field field = begin
		
	end in List.iter output_field message.fields;
	printf "\tList.iter fn msg.unknown;\n";
	printf "end\n\n"
end ;;

let output_to_int enum = begin
	printf "and %s (value : %s) = begin\n"
		(fq_type_to_int_name enum.enum_fq_type)
		(fq_type_name enum.enum_fq_type);
	printf "\tmatch value with\n";
	let output_enum_value value = begin
		printf "\t| %s -> %s\n" value.enum_value_name (Int32.to_string value.value);
	end in List.iter output_enum_value enum.values;
	printf "end\n\n";
end ;;

let output_of_int enum = begin
	printf "and %s : (int -> %s) = fun value -> begin\n"
		(fq_type_of_int_name enum.enum_fq_type)
		(fq_type_name enum.enum_fq_type);
	printf "\tmatch value with\n";
	let output_enum_value value = begin
		printf "\t| %s -> %s\n" (Int32.to_string value.value) value.enum_value_name;
	end in List.iter output_enum_value enum.values;
	printf "\t| _ -> raise (Proto.Invalid_parse \"unknown enum value\")\n";
	printf "end\n\n";
end ;;

let rec output_message_impl message = begin
	output_empty message;
	output_update message;
	output_iter message;

	List.iter output_message_impl message.sub_messages;
	List.iter output_enum_impl message.sub_enums;
end and output_enum_impl enum = begin
	output_to_int enum;
	output_of_int enum;
end ;;

let output_impls file = begin
	printf "let rec empty = ()\n\n" ;
	List.iter output_message_impl file.messages;
	List.iter output_enum_impl file.enums;
	printf ";;\n";
end ;;

let output_file file = begin
	output_types file ;
	output_impls file ;
end ;;

let output_files files = begin
	printf "open Common ;;\n";
	printf "open Stdint ;;\n";
	printf "\n";
	List.iter output_file files;
end ;;
