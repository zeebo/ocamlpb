open Stdint ;;

type empty = unit

and pEnumValueDescriptorProto = {
	unknown : Proto.field list ;
	_number : Int32.t option ;
	_name : string option ;
}

and pEnumDescriptorProto = {
	unknown : Proto.field list ;
	_value : pEnumValueDescriptorProto list ;
	_name : string option ;
}

and pFieldDescriptorProto = {
	unknown : Proto.field list ;
	_default_value : string option ;
	_type_name : string option ;
	_type : pFieldDescriptorProto_Type option ;
	_label : pFieldDescriptorProto_Label option ;
	_number : Int32.t option ;
	_name : string option ;
}

and pFieldDescriptorProto_Label =
	| LABEL_REPEATED
	| LABEL_REQUIRED
	| LABEL_OPTIONAL

and pFieldDescriptorProto_Type =
	| TYPE_SINT64
	| TYPE_SINT32
	| TYPE_SFIXED64
	| TYPE_SFIXED32
	| TYPE_ENUM
	| TYPE_UINT32
	| TYPE_BYTES
	| TYPE_MESSAGE
	| TYPE_GROUP
	| TYPE_STRING
	| TYPE_BOOL
	| TYPE_FIXED32
	| TYPE_FIXED64
	| TYPE_INT32
	| TYPE_UINT64
	| TYPE_INT64
	| TYPE_FLOAT
	| TYPE_DOUBLE

and pDescriptorProto = {
	unknown : Proto.field list ;
	_enum_type : pEnumDescriptorProto list ;
	_nested_type : pDescriptorProto list ;
	_field : pFieldDescriptorProto list ;
	_name : string option ;
}

and pFileDescriptorProto = {
	unknown : Proto.field list ;
	_enum_type : pEnumDescriptorProto list ;
	_message_type : pDescriptorProto list ;
	_package : string option ;
	_name : string option ;
}

and pFileDescriptorSet = {
	unknown : Proto.field list ;
	_file : pFileDescriptorProto list ;
}

let rec empty = ()

and pEnumValueDescriptorProto_empty : pEnumValueDescriptorProto = {
	unknown = [];
	_number = None ;
	_name = None ;
}

and pEnumValueDescriptorProto_update (msg : pEnumValueDescriptorProto) field = begin
	match field with
	| (2, Proto.Varint value) ->
		let decoded = (Int32.of_uint64 value) in
		{ msg with _number = Some decoded }
	| (2, _) -> invalid_arg "wrong type for field"
	| (1, Proto.LengthDelimited value) ->
		let decoded = (Bytes.to_string value) in
		{ msg with _name = Some decoded }
	| (1, _) -> invalid_arg "wrong type for field"
	| _ -> { msg with unknown = field :: msg.unknown }
end

and pEnumValueDescriptorProto_iter (msg : pEnumValueDescriptorProto) fn = begin
	List.iter fn msg.unknown;
end

and pEnumDescriptorProto_empty : pEnumDescriptorProto = {
	unknown = [];
	_value = [] ;
	_name = None ;
}

and pEnumDescriptorProto_update (msg : pEnumDescriptorProto) field = begin
	match field with
	| (2, Proto.LengthDelimited value) ->
		let decoded = (Proto.unmarshal value pEnumValueDescriptorProto_empty pEnumValueDescriptorProto_update) in
		{ msg with _value = decoded :: msg._value }
	| (2, _) -> invalid_arg "wrong type for field"
	| (1, Proto.LengthDelimited value) ->
		let decoded = (Bytes.to_string value) in
		{ msg with _name = Some decoded }
	| (1, _) -> invalid_arg "wrong type for field"
	| _ -> { msg with unknown = field :: msg.unknown }
end

and pEnumDescriptorProto_iter (msg : pEnumDescriptorProto) fn = begin
	List.iter fn msg.unknown;
end

and pFieldDescriptorProto_empty : pFieldDescriptorProto = {
	unknown = [];
	_default_value = None ;
	_type_name = None ;
	_type = None ;
	_label = None ;
	_number = None ;
	_name = None ;
}

and pFieldDescriptorProto_update (msg : pFieldDescriptorProto) field = begin
	match field with
	| (7, Proto.LengthDelimited value) ->
		let decoded = (Bytes.to_string value) in
		{ msg with _default_value = Some decoded }
	| (7, _) -> invalid_arg "wrong type for field"
	| (6, Proto.LengthDelimited value) ->
		let decoded = (Bytes.to_string value) in
		{ msg with _type_name = Some decoded }
	| (6, _) -> invalid_arg "wrong type for field"
	| (5, Proto.Varint value) ->
		let decoded = (pFieldDescriptorProto_Type_of_int @@ Uint64.to_int value) in
		{ msg with _type = Some decoded }
	| (5, _) -> invalid_arg "wrong type for field"
	| (4, Proto.Varint value) ->
		let decoded = (pFieldDescriptorProto_Label_of_int @@ Uint64.to_int value) in
		{ msg with _label = Some decoded }
	| (4, _) -> invalid_arg "wrong type for field"
	| (3, Proto.Varint value) ->
		let decoded = (Int32.of_uint64 value) in
		{ msg with _number = Some decoded }
	| (3, _) -> invalid_arg "wrong type for field"
	| (1, Proto.LengthDelimited value) ->
		let decoded = (Bytes.to_string value) in
		{ msg with _name = Some decoded }
	| (1, _) -> invalid_arg "wrong type for field"
	| _ -> { msg with unknown = field :: msg.unknown }
end

and pFieldDescriptorProto_iter (msg : pFieldDescriptorProto) fn = begin
	List.iter fn msg.unknown;
end

and pFieldDescriptorProto_Label_to_int (value : pFieldDescriptorProto_Label) = begin
	match value with
	| LABEL_REPEATED -> 3
	| LABEL_REQUIRED -> 2
	| LABEL_OPTIONAL -> 1
end

and pFieldDescriptorProto_Label_of_int : (int -> pFieldDescriptorProto_Label) = fun value -> begin
	match value with
	| 3 -> LABEL_REPEATED
	| 2 -> LABEL_REQUIRED
	| 1 -> LABEL_OPTIONAL
	| _ -> raise (Proto.Invalid_parse "unknown enum value")
end

and pFieldDescriptorProto_Type_to_int (value : pFieldDescriptorProto_Type) = begin
	match value with
	| TYPE_SINT64 -> 18
	| TYPE_SINT32 -> 17
	| TYPE_SFIXED64 -> 16
	| TYPE_SFIXED32 -> 15
	| TYPE_ENUM -> 14
	| TYPE_UINT32 -> 13
	| TYPE_BYTES -> 12
	| TYPE_MESSAGE -> 11
	| TYPE_GROUP -> 10
	| TYPE_STRING -> 9
	| TYPE_BOOL -> 8
	| TYPE_FIXED32 -> 7
	| TYPE_FIXED64 -> 6
	| TYPE_INT32 -> 5
	| TYPE_UINT64 -> 4
	| TYPE_INT64 -> 3
	| TYPE_FLOAT -> 2
	| TYPE_DOUBLE -> 1
end

and pFieldDescriptorProto_Type_of_int : (int -> pFieldDescriptorProto_Type) = fun value -> begin
	match value with
	| 18 -> TYPE_SINT64
	| 17 -> TYPE_SINT32
	| 16 -> TYPE_SFIXED64
	| 15 -> TYPE_SFIXED32
	| 14 -> TYPE_ENUM
	| 13 -> TYPE_UINT32
	| 12 -> TYPE_BYTES
	| 11 -> TYPE_MESSAGE
	| 10 -> TYPE_GROUP
	| 9 -> TYPE_STRING
	| 8 -> TYPE_BOOL
	| 7 -> TYPE_FIXED32
	| 6 -> TYPE_FIXED64
	| 5 -> TYPE_INT32
	| 4 -> TYPE_UINT64
	| 3 -> TYPE_INT64
	| 2 -> TYPE_FLOAT
	| 1 -> TYPE_DOUBLE
	| _ -> raise (Proto.Invalid_parse "unknown enum value")
end

and pDescriptorProto_empty : pDescriptorProto = {
	unknown = [];
	_enum_type = [] ;
	_nested_type = [] ;
	_field = [] ;
	_name = None ;
}

and pDescriptorProto_update (msg : pDescriptorProto) field = begin
	match field with
	| (4, Proto.LengthDelimited value) ->
		let decoded = (Proto.unmarshal value pEnumDescriptorProto_empty pEnumDescriptorProto_update) in
		{ msg with _enum_type = decoded :: msg._enum_type }
	| (4, _) -> invalid_arg "wrong type for field"
	| (3, Proto.LengthDelimited value) ->
		let decoded = (Proto.unmarshal value pDescriptorProto_empty pDescriptorProto_update) in
		{ msg with _nested_type = decoded :: msg._nested_type }
	| (3, _) -> invalid_arg "wrong type for field"
	| (2, Proto.LengthDelimited value) ->
		let decoded = (Proto.unmarshal value pFieldDescriptorProto_empty pFieldDescriptorProto_update) in
		{ msg with _field = decoded :: msg._field }
	| (2, _) -> invalid_arg "wrong type for field"
	| (1, Proto.LengthDelimited value) ->
		let decoded = (Bytes.to_string value) in
		{ msg with _name = Some decoded }
	| (1, _) -> invalid_arg "wrong type for field"
	| _ -> { msg with unknown = field :: msg.unknown }
end

and pDescriptorProto_iter (msg : pDescriptorProto) fn = begin
	List.iter fn msg.unknown;
end

and pFileDescriptorProto_empty : pFileDescriptorProto = {
	unknown = [];
	_enum_type = [] ;
	_message_type = [] ;
	_package = None ;
	_name = None ;
}

and pFileDescriptorProto_update (msg : pFileDescriptorProto) field = begin
	match field with
	| (5, Proto.LengthDelimited value) ->
		let decoded = (Proto.unmarshal value pEnumDescriptorProto_empty pEnumDescriptorProto_update) in
		{ msg with _enum_type = decoded :: msg._enum_type }
	| (5, _) -> invalid_arg "wrong type for field"
	| (4, Proto.LengthDelimited value) ->
		let decoded = (Proto.unmarshal value pDescriptorProto_empty pDescriptorProto_update) in
		{ msg with _message_type = decoded :: msg._message_type }
	| (4, _) -> invalid_arg "wrong type for field"
	| (2, Proto.LengthDelimited value) ->
		let decoded = (Bytes.to_string value) in
		{ msg with _package = Some decoded }
	| (2, _) -> invalid_arg "wrong type for field"
	| (1, Proto.LengthDelimited value) ->
		let decoded = (Bytes.to_string value) in
		{ msg with _name = Some decoded }
	| (1, _) -> invalid_arg "wrong type for field"
	| _ -> { msg with unknown = field :: msg.unknown }
end

and pFileDescriptorProto_iter (msg : pFileDescriptorProto) fn = begin
	List.iter fn msg.unknown;
end

and pFileDescriptorSet_empty : pFileDescriptorSet = {
	unknown = [];
	_file = [] ;
}

and pFileDescriptorSet_update (msg : pFileDescriptorSet) field = begin
	match field with
	| (1, Proto.LengthDelimited value) ->
		let decoded = (Proto.unmarshal value pFileDescriptorProto_empty pFileDescriptorProto_update) in
		{ msg with _file = decoded :: msg._file }
	| (1, _) -> invalid_arg "wrong type for field"
	| _ -> { msg with unknown = field :: msg.unknown }
end

and pFileDescriptorSet_iter (msg : pFileDescriptorSet) fn = begin
	List.iter fn msg.unknown;
end

;;